import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TableModule } from 'ngx-easy-table';

import { appRoutes } from './app.routerConfig';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { LandingAuthenticationComponent } from './landing/landing-authenticationh/landing-authentication.component';
import { AppService } from './core/app.service';
import { AuthCurrentService } from './core/auth.current.service';
import { RestAuthService } from './core/app.rest.service';
import { AuthService } from './core/auth.service';
import { ConfigService } from './core/config.service';
import { CanActivateChildAuthGuard } from './core/CanActivateChildAuthGuard.service';
import { AuthInterceptor } from './core/auth-interceptor';
import { CommunicationService } from './core/app.communication.service';
import { AdminComponent } from './admin/admin.component';
import { AdminHeaderComponent } from './admin/commons/admin-header/admin-header.component';
import { AdminMenuComponent } from './admin/commons/admin-menu/admin-menu.component';
import { AdminAuthenticationComponent } from './admin/commons/admin-authentication/admin-authentication.component';
import { Test1Component } from './admin/components/test1/test1.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { UsermanagementComponent } from './admin/components/usermanagement/usermanagement.component';
import { AuthmanagementComponent } from './admin/components/authmanagement/authmanagement.component';
import { CommunicationComponent } from './admin/commons/communication/communication.component';
import { UserdetailComponent } from './admin/components/usermanagement/userdetail/userdetail.component';
import { ComponentdetailComponent } from './admin/components/authmanagement/componentdetail/componentdetail.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LandingAuthenticationComponent,
    AdminComponent,
    AdminHeaderComponent,
    AdminMenuComponent,
    AdminAuthenticationComponent,
    Test1Component,
    PagenotfoundComponent,
    UsermanagementComponent,
    AuthmanagementComponent,
    CommunicationComponent,
    UserdetailComponent,
    ComponentdetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TableModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    AppService, AuthCurrentService, RestAuthService, AuthService, ConfigService, CanActivateChildAuthGuard, CommunicationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
