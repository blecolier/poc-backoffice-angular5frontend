import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LandingAuthenticationComponent } from './landing-authentication.component';

describe('LandingAuthenticationhComponent', () => {
  let component: LandingAuthenticationComponent;
  let fixture: ComponentFixture<LandingAuthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingAuthenticationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
