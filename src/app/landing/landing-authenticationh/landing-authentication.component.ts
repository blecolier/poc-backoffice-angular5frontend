import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from '../../core/auth.service'
import {AppService} from '../../core/app.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-authentication',
  templateUrl: './landing-authentication.component.html',
  styleUrls: ['./landing-authentication.component.css']
})
export class LandingAuthenticationComponent implements OnInit {
  loginForm: FormGroup;
  loginErrorMessage: string;
  loginError : boolean;

  constructor(fb: FormBuilder, private authService: AuthService, private router: Router, private appService: AppService) {
    this.loginForm = fb.group({'username': ['', Validators.required], 'password': ['', Validators.required]});
  }

  ngOnInit() {
  }

  onSubmit() {
    if(this.loginForm.controls['username'].hasError('required') || this.loginForm.controls['password'].hasError('required')){
      this.loginErrorMessage = 'Missing user or password';
      this.loginError = true;
      return;
    }

    this.authService.authenticate(this.loginForm.value, res => {
      if (res) {
        this.appService.userLogedIn();
        this.loginError = false;
        this.router.navigate(['/admin']);
      }
      else {
        this.loginErrorMessage = 'Wrong user or password';
        this.loginError = true;
      }
    });
  }
}
