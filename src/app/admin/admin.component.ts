import { Component, OnInit, OnDestroy } from '@angular/core';

// Variable defined in /assets/js/scripts.js
declare var AdminLTE: any;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {
  bodyClasses = 'skin-blue';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];

  constructor() { }

  ngOnInit() {
    this.body.classList.add('skin-blue');
    AdminLTE.init();
  }

  ngOnDestroy() {
    this.body.classList.remove('skin-blue');
}
}
