import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../core/app.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {
  componentTitle: string;

  constructor(private appService: AppService) {
    appService.titleChanged.subscribe(title => this.componentTitle = title);
  }

  ngOnInit() {
  }

}
