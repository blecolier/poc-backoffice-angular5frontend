import { Component, OnInit } from '@angular/core';
import {AppService} from '../../../core/app.service'
import {AuthService} from '../../../core/auth.service'

@Component({
  selector: 'app-admin-authentication',
  templateUrl: './admin-authentication.component.html',
  styleUrls: ['./admin-authentication.component.css']
})
export class AdminAuthenticationComponent implements OnInit {
  UserName: string;
  authService: AuthService;

  constructor(private appService: AppService, authService: AuthService) {
    this.authService = authService;
    this.UserName = this.appService.getUserName();
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }
}
