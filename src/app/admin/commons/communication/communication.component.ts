import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef } from '@angular/core';
import { AppService } from '../../../core/app.service';
import { Alert, AlertType } from '../../../core/model/alert-type';
import { CommunicationService } from '../../../core/app.communication.service';

// Variable defined in /assets/js/scripts.js
declare var jquery: any;

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css']
})
export class CommunicationComponent implements OnInit {
  alerts: Map<AlertType, Alert> = new Map<AlertType, Alert>();
  confirmationInfo: Alert;

  constructor(private communicationService: CommunicationService, private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.communicationService.getAlert()
    .subscribe(alert => {

      if(!this.alerts.has(alert.type))
        this.alerts.set(alert.type, alert);
      else{
        alert.messages.forEach(value => this.alerts.get(alert.type).messages.push(value));
      }

      this.cdr.detectChanges();
      jquery.setTimeout(3, () => { this.removeAlertType(alert.type) });
    });

    this.communicationService.getModal()
    .subscribe(confirm => {
      this.confirmationInfo = confirm;
      jquery.showModal('confirmationModal');
      this.cdr.detectChanges();
    });
  }

  removeAlertType(type: AlertType){
    this.alerts.delete(type);
  }

  getBootsrapClass(type: AlertType) : string{
    switch (type) {
      case AlertType.Success:
        return 'alert alert-success';
      case AlertType.Error:
        return 'alert alert-danger';
      case AlertType.Info:
        return 'alert alert-info';
      }
  }

  getBootsrapIcon(type: AlertType) : string{
    switch (type) {
      case AlertType.Success:
        return 'fa fa-check-circle';
      case AlertType.Error:
        return 'fa fa-exclamation-triangle';
      case AlertType.Info:
        return 'fa fa-info-circle';
      }
  }

  confirmCallback(){
    this.confirmationInfo.callback();
    jquery.hideModal('confirmationModal');
  }
}
