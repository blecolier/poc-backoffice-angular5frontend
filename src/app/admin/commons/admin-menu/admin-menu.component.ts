import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {RestAuthService} from '../../../core/app.rest.service'
import {AppService} from '../../../core/app.service'
import {Utils} from '../../../core/utils'

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.css']
})
export class AdminMenuComponent implements OnInit {
  userModules: Array<any>;
  modules: Array<any>;

  constructor(private restAuthService: RestAuthService, private appservice: AppService, private router: Router) {
    this.userModules = this.appservice.getUserModules();
  }

  ngOnInit() {
    this.getModuleData();
  }

  getModuleData() {
    this.restAuthService.get('component/active')
      .then(data => {
        let temp = [];

        for (let j = 0; j < data.components.length; j++) {
          for (let i = 0; i < this.userModules.length; i++) {
            if (this.userModules[i].moduleName.indexOf(data.components[j].componentName) > -1) {
              temp.push(data.components[j]);
            }
          }
        }

        this.modules = Utils.groupBy(temp, 'category').sort(x => x.value.categoryOrder);
      }, error => {
        console.log('Error while obtaining comopnent list', error);
      });
  }
}
