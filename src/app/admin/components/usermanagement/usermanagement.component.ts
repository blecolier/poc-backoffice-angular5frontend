import { Component, OnInit } from '@angular/core';

import { AppService } from '../../../core/app.service';
import { RestAuthService } from '../../../core/app.rest.service';
import { CommunicationService } from '../../../core/app.communication.service';
import { AdminComponent } from '../../../core/admincomponent';
import { ConfigService, EventObject, PaginationFactory } from '../../../core/gridconfig.service';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.css'],
  providers: [ConfigService]
})
export class UsermanagementComponent extends AdminComponent implements OnInit {
  users: any[];
  currentUser: any;
  userSelected: boolean;
  modules: any[];

  columns = [
    { key: 'name', title: 'User' },
    { key: 'displayName', title: 'Name' },
    { key: 'role', title: 'Role' },
    { key: 'isActive', title: 'Active' },
    { key: 'modules', title: 'Modules' },
    { key: 'actions', title: 'Actions' }
  ];
  data;
  configuration;
  pagination = {limit: 10, offset: 0, count: null };

  constructor(appService: AppService, comService: CommunicationService, private restAuthService: RestAuthService) {
    super(appService, comService, 'User Management');
  }

  ngOnInit() {
    this.configuration = ConfigService.config;
    this.refreshUserList('loaded', PaginationFactory.CreatePagination(this.pagination));
    this.loadModules();
  }

  eventEmitted(event: EventObject) {
    console.log(event);

    this.pagination.limit = event.value.limit ? event.value.limit : this.pagination.limit;
    this.pagination.offset = event.value.page ? event.value.offset : this.pagination.offset;
    this.pagination = { ...this.pagination };

    this.refreshUserList('loaded', PaginationFactory.CreatePagination(this.pagination));
  }


  loadModules(){
    this.restAuthService.get(`component/active`)
      .then(res => {
        this.modules = res.components;
      });
  }

  refreshUserList(event: String, params: String) {
    this.configuration.isLoading = true;
    this.restAuthService.get(`user?${params}`)
      .then(res => {
        this.users = res.paginationResult.docs;
        this.data = res.paginationResult.docs;

        this.pagination.count = res.paginationResult.total;
        this.pagination = { ...this.pagination };
        this.configuration.isLoading = false;

        if(event === 'deleted'){
          this.userSelected = false;
          this.currentUser = null;
        }
      })
      .catch(error => {
        this.communicationService.error('Error while obtaining user list', [error]);
      });
  }

  getModules(user) {
    if (user !== null && typeof user !== 'undefined') {
      let modules = '';
      user.modules.forEach(function (item) {
        if (typeof item.moduleName !== 'undefined') {
            modules = `${modules} ${item.moduleName} `;
        }
      });

      if (modules.length > 40) {
          modules = `${modules.substring(0, 30)}...`;
      }
      return modules;
    }
  }

  onSelected(user){
    this.currentUser = user.user;
    this.userSelected = true;
  }
}
