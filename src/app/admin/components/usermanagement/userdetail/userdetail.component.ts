import { Component, Input, OnInit, Output, OnChanges, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

import { RestAuthService } from '../../../../core/app.rest.service';
import { FormHelper } from '../../../../core/formHelper';
import {Utils} from '../../../../core/utils'
import { CommunicationService } from '../../../../core/app.communication.service';

// Variable defined in /assets/js/scripts.js
declare var jquery: any;

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css']
})
export class UserdetailComponent implements OnInit, OnChanges {
  @Input() user : any;
  @Input() modules: any[];
  @Output() onComponentmodified: EventEmitter<string> = new EventEmitter()
  userForm: FormGroup;
  rightList: FormGroup;

  constructor(private fb: FormBuilder, private comService: CommunicationService, private restAuthService: RestAuthService) {
  }

  ngOnInit(){
  }

  private createAccessRightFormControls(){
    let groupedModules = Utils.groupBy(this.modules, 'category');

    this.rightList = this.fb.group({
      category : this.fb.array([])
    });

    let categoryArray = this.rightList.get('category') as FormArray;

    groupedModules.forEach(cat => {
      let category = this.fb.group({
        category: [cat.key],
        modules: this.fb.array([])
      });

      let moduleArray = category.get('modules') as FormArray;

      cat.value.forEach(module => {
        let userModule = this.user.modules.find(x => x.moduleName === module.componentName);

        let moduleAccess = this.fb.group({
          moduleName: [module.componentName],
          read: [userModule !== undefined ? userModule.permissions.read : false],
          modify: [userModule !== undefined ? userModule.permissions.modify : false],
          delete: [userModule !== undefined ? userModule.permissions.delete : false],
          audit: [userModule !== undefined ? userModule.permissions.audit : false],
        });

        moduleArray.push(moduleAccess);
      });

      categoryArray.push(category);
    });
  }

  ngOnChanges(changes)
  {
    this.createAccessRightFormControls();

    console.log(this.rightList);

    this.userForm = this.fb.group({
      role : [this.user.role, Validators.required],
      displayName : [this.user.displayName, Validators.required],
      name : [this.user.name, Validators.required],
      isActive : [this.user.isActive],
      modules : [this.rightList]
      });
  }

  private hasrights(module){
    return module.read || module.modify || module.delete || module.audit;
  }

  modify(){
    FormHelper.checkFormForErrors(this.userForm)
    .then(data => {
      let userValue = this.userForm.value;

      this.user.name = userValue.name;
      this.user.role = userValue.role;
      this.user.displayName = userValue.displayName;
      this.user.isActive = userValue.isActive;

      let result = [];

      userValue.modules.value.category.forEach(category => {
          category.modules.forEach(module => {
            if(this.hasrights(module)){
              result.push({moduleName: module.moduleName, permissions: {read: module.read, modify: module.modify, delete: module.delete, audit: module.audit}})
            }
          })
      });

      this.user.modules = result;

      this.restAuthService.put(`user/${this.user._id}`, this.user)
        .then(() => {
          this.comService.success('', ['user successfully edited']);
          this.onComponentmodified.emit('modified');
        })
        .catch(error => {
          this.comService.error('Error while editing the user', error);
        });

    })
    .catch(messageList => {
      this.comService.error('Some Data are missing', messageList);
    });
  }

  confirmdelete(){
    this.comService.confirm('Beware of not losing data', 'You are about to delete this entry; are you sure you want to continue?',
      () => {
        this.restAuthService.delete(`user/${this.user._id}`)
          .then(() => {
            this.comService.success('User deleted', ['User deleted successfully']);
            this.onComponentmodified.emit('deleted');
          })
          .catch(error => {
            this.comService.error('Error eliminando el componente', error);
        });
      });
  }
}
