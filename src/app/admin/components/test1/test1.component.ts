import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../core/app.service';
import { AdminComponent } from '../../../core/admincomponent';
import { CommunicationService } from '../../../core/app.communication.service';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component extends AdminComponent implements OnInit {

  constructor(appService: AppService, comService: CommunicationService) {
    super(appService, comService, 'Test 1');
  }

  ngOnInit() {
  }

}
