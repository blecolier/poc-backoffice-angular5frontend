import { Component, Input, Output, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FormValidationResult } from '../../../../core/model/formValidationResult';
import { FormHelper } from '../../../../core/formHelper';
import { RestAuthService } from '../../../../core/app.rest.service';
import { CommunicationService } from '../../../../core/app.communication.service';

@Component({
  selector: 'app-componentdetail',
  templateUrl: './componentdetail.component.html',
  styleUrls: ['./componentdetail.component.css']
})
export class ComponentdetailComponent implements OnInit, OnChanges {
  @Input() component : any;
  @Output() onComponentmodified: EventEmitter<string> = new EventEmitter()
  componentForm: FormGroup;

  constructor(private fb: FormBuilder, private comService: CommunicationService, private restAuthService: RestAuthService) {

  }

  ngOnInit() {
  }

  ngOnChanges(changes){
    this.initializeform();
  }

  isNewComponent(){
    return this.component !== null && this.component.menuName === '';
  }

  initializeform(){
    this.componentForm = this.fb.group({
      menuName : [this.component.menuName, Validators.required],
      componentName : [this.component.componentName, Validators.required],
      controllerName : [this.component.controllerName, Validators.required],
      role : [this.component.role, Validators.required],
      isActive : [this.component.isActive],
      category : [this.component.category, Validators.required],
      iconClass : [this.component.iconClass],
      categoryOrder : [this.component.categoryOrder, Validators.required],
      menuOrder : [this.component.menuOrder, Validators.required]
    });
  }

  confirmClear(){
    this.comService.confirm('Beware of not losing data', 'All the data will be deleted; are you sure you want to continue?',
      () => {
        this.componentForm.reset();
      }
    );
  }

  save(){
    FormHelper.checkFormForErrors(this.componentForm)
      .then(data => {
        this.restAuthService.post('component', data)
              .then(() => {
                this.comService.success('', ['Componente guardado']);
                this.onComponentmodified.emit('saved');
              })
              .catch(error => {
                this.comService.error('Error guardando el componente', error);
              });
      })
      .catch(messageList => {
        this.comService.error('Some Data are missing', messageList);
      });
  }

  modify(){
    FormHelper.checkFormForErrors(this.componentForm)
      .then(data => {
        this.restAuthService.put(`component/${this.component._id}`, data)
          .then(() => {
            this.comService.success('', ['Component modified']);
            this.onComponentmodified.emit('modified');
          })
          .catch(error => {
            this.comService.error('An error occured while modifying the component', error);
          });
      })
      .catch(messageList => {
        this.comService.error('Error during component modification', messageList);
      });
  }

  confirmdelete(){
    this.comService.confirm('Beware of not losing data', 'All the data will be deleted; are you sure you want to continue?',
      () => {
        this.restAuthService.delete(`component/${this.component._id}`)
         .then(() => {
           this.comService.success('Component deleted', ['Component deleted successfully']);
           this.componentForm.reset();
           this.onComponentmodified.emit('deleted');
         })
        .catch(error => {
           this.comService.error('Error during component modification', error);
        });
      }
    );
  }

}
