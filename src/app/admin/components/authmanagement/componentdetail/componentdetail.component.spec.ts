import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentdetailComponent } from './componentdetail.component';

describe('ComponentdetailComponent', () => {
  let component: ComponentdetailComponent;
  let fixture: ComponentFixture<ComponentdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
