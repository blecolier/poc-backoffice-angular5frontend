import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthmanagementComponent } from './authmanagement.component';

describe('AuthmanagementComponent', () => {
  let component: AuthmanagementComponent;
  let fixture: ComponentFixture<AuthmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
