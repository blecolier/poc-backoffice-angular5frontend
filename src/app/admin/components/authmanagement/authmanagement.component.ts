import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../core/app.service';
import { AdminComponent } from '../../../core/admincomponent';
import { CommunicationService } from '../../../core/app.communication.service';
import { RestAuthService } from '../../../core/app.rest.service';
import { ConfigService, EventObject, PaginationFactory } from '../../../core/gridconfig.service';

@Component({
  selector: 'app-authmanagement',
  templateUrl: './authmanagement.component.html',
  styleUrls: ['./authmanagement.component.css'],
  providers: [ConfigService]
})
export class AuthmanagementComponent extends AdminComponent implements OnInit {
  components: any[];
  currentComponent: any;
  componentSelected: boolean;

  columns = [
    { key: 'componentName', title: 'Component' },
    { key: 'isActive', title: 'Active' },
    { key: 'categoryOrder', title: 'Category' },
    { key: 'menuOrder', title: 'Order' },
    { key: 'actions', title: 'Actions' }
  ];
  data;
  configuration;
  pagination = {limit: 10, offset: 0, count: null };

  constructor(appService: AppService, comService: CommunicationService, private restAuthService: RestAuthService) {
    super(appService, comService, 'Component Management');
  }

  ngOnInit() {
    this.configuration = ConfigService.config;
    this.refreshComponentList('Loaded', PaginationFactory.CreatePagination(this.pagination));
  }

  eventEmitted(event: EventObject) {
    this.pagination.limit = event.value.limit ? event.value.limit : this.pagination.limit;
    this.pagination.offset = event.value.page ? (event.value.page - 1) * this.pagination.limit : this.pagination.offset;
    this.pagination = { ...this.pagination };

    this.refreshComponentList('Loaded', PaginationFactory.CreatePagination(this.pagination));
  }

  refreshComponentList(event :string, params: string) {
    this.configuration.isLoading = true;
    this.restAuthService.get(`component?${params}`)
      .then(res => {
        this.components = res.paginationResult.docs;
        this.data = res.paginationResult.docs;
        this.pagination.count = res.paginationResult.total;
        this.pagination = { ...this.pagination };
        this.configuration.isLoading = false;

        if(event === 'deleted'){
          this.componentSelected = false;
          this.currentComponent = null;
        }
      })
      .catch(error => {
        this.communicationService.error('Error while obtaining component list', [error]);
      });
    }

    onSelected(component){
      this.currentComponent = component.component;
      this.componentSelected = true;
    }

    createNew(){
      this.currentComponent = {
        menuName : '',
        componentName : '',
        controllerName : '',
        role : '',
        isActive : '',
        category : '',
        iconClass : '',
        categoryOrder : '',
        menuOrder : '',
      };

      this.componentSelected = true;
    }

    onDeleted(component){
      this.communicationService.confirm('Beware of not losing data', 'You are about to delete this entry; are you sure you want to continue?',
        () => {
          this.restAuthService.delete(`component/${component.component._id}`)
            .then(() => {
              this.communicationService.success('Component deleted', ['Component deleted successfully']);
              this.refreshComponentList('deleted', PaginationFactory.CreatePagination(this.pagination));
            })
            .catch(error => {
              this.communicationService.error('Error eliminando el componente', error);
          });
        });
    }
}
