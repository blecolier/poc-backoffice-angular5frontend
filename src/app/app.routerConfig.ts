import { Routes } from '@angular/router';
import { CanActivateChildAuthGuard } from './core/CanActivateChildAuthGuard.service';

import { LandingComponent } from './landing/landing.component';
import { AdminComponent } from './admin/admin.component';
import { Test1Component } from './admin/components/test1/test1.component';
import { UsermanagementComponent } from './admin/components/usermanagement/usermanagement.component';
import { AuthmanagementComponent } from './admin/components/authmanagement/authmanagement.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: LandingComponent
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivateChild: [CanActivateChildAuthGuard],
    children: [
      {
        path: '',
        component: Test1Component,
        pathMatch: 'full'
      },
      {
        path: 'test1',
        component: Test1Component
      },
      {
        path: 'usermanagement',
        component: UsermanagementComponent
      },
      {
        path: 'authmanagement',
        component: AuthmanagementComponent
      }
    ]
  },
  { path: '**', component: PagenotfoundComponent }
];
