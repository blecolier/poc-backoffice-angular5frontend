import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

declare const Buffer;

@Injectable()
export class AuthCurrentService{
  constructor(private router: Router){
    this.router = router;
  }

  getAuthenticationHeader(){
    let currentToken = localStorage.jwtToken;
    return currentToken != null && currentToken != '' ? 'JWT ' + currentToken : null;
  }

  extractUserFromToken(){
    let user = new Buffer(localStorage.jwtToken.split('.')[1], 'base64').toString();
    return JSON.parse(user.toString());
  }

  setCurrentToken(token){
    localStorage.jwtToken = token;
    localStorage.jwtTokenExpirationDate = token === '' ? 0 : this.extractUserFromToken().exp;
  }

  destroyToken(){
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('jwtTokenExpirationDate');
    localStorage.removeItem('currentUser');
    localStorage.logedIn = false;

    this.router.navigate(['/']);
  }

  getCurrentToken(){
    return localStorage.jwtToken;
  }

  getTokenExpirationDate(){
    return localStorage.jwtTokenExpirationDate;
  }
}
