import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormValidationResult } from './model/formValidationResult';

export class FormHelper{

  static checkFormForErrors(formGroup: FormGroup): Promise<any>{
    let res: FormValidationResult = new FormValidationResult;

    Object.keys(formGroup.controls).forEach(key => {
        if(formGroup.controls[key].hasError('required')){
          res.isValid = false;
          res.messages.push(`Field ${key} id required`);
        }
    });

    return res.isValid ? Promise.resolve(formGroup.value) : Promise.reject(res.messages);
  }
}
