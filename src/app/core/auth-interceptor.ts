import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { AuthCurrentService } from './auth.current.service';
import {ConfigService} from './config.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private apiVersion: string;

  constructor(private authService: AuthCurrentService, appConfig: ConfigService){
    appConfig.getConfiguration()
      .subscribe(data => {
        this.apiVersion = data.Api.version;
      }, error => console.log(error));
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith(`appConfig.Api.baseUrl}/${this.apiVersion}/`) &&
      (!req.headers.has('Authorization'))) {
      console.log("authentification information missing");
      return Observable.throw(req);
    }

    return next.handle(req)
      .do(event => {
        if (event instanceof HttpResponse) {

          if (event.status === undefined) {
            this.authService.setCurrentToken('');
            window.location.reload();
            return Observable.throw(event);
          }

          // Rejected from request
          if (!event.headers.has('Authorization') /*&& event.headers. method*/) {
            return Observable.throw(event);
          }

          if (event.status === 403 && !event.ok) {
            console.log(event.status);
            this.authService.setCurrentToken('');
            return Observable.throw(event);
          }

          console.log('let it go!')
          console.log(event.status);
        }

        if(event instanceof HttpErrorResponse){
          console.log("Error Caught By Interceptor");
          console.log(event.status);
          if (event.status === 401 || event.status === 403)
          {
            console.log('401 error');
            this.authService.setCurrentToken('');
          window.location.reload();}
        }
      },
      err => {
        if(err instanceof HttpErrorResponse){
          console.log("Error Caught By Interceptor");
          console.log(err.status);
          if (err.status === 401 || err.status === 403)
          {this.authService.setCurrentToken('');
          window.location.reload();}
        }
      });
  }
}
