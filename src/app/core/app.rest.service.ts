import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import {ConfigService} from './config.service';

@Injectable()
export class RestAuthService{
  private baseUrl: string;
  private apiVersion: string;

  constructor(private authService: AuthService, private http: HttpClient, appConfig: ConfigService){
    appConfig.getConfiguration()
      .subscribe(data => {
        this.baseUrl = data.Api.baseUrl;
        this.apiVersion = data.Api.version;
      }, error => console.log(error));
  }

  addAuthHeader(url: string, token: string, apiVersion: string){
    if(token !== null){
      let headers = new HttpHeaders().set('Authorization', token);
      return { headers:headers };
    }

    return {};
  }

  post(url, data){
    return this.authService.refreshTokenIfNecessary()
      .then(() => {
        let config = this.addAuthHeader(url, this.authService.getAuthenticationHeader(), this.apiVersion);
        return this.http.post(`${this.baseUrl}/${this.apiVersion}/${url}`, data, config)
          .map((res:any) => res).toPromise();
      }
    );
  }

  get(url){
    return this.authService.refreshTokenIfNecessary()
      .then(() => {
        let config = this.addAuthHeader(url, this.authService.getAuthenticationHeader(), this.apiVersion);
        return this.http.get(`${this.baseUrl}/${this.apiVersion}/${url}`, config)
          .map((res:any) => res).toPromise();
      }
    );
  }

  put(url, data){
    return this.authService.refreshTokenIfNecessary()
      .then(() => {
        let config = this.addAuthHeader(url, this.authService.getAuthenticationHeader(), this.apiVersion);
        return this.http.put(`${this.baseUrl}/${this.apiVersion}/${url}`, data, config)
          .map((res:any) => res).toPromise();
      }
    );
  }

  delete(url){
    return this.authService.refreshTokenIfNecessary()
      .then(() => {
        let config = this.addAuthHeader(url, this.authService.getAuthenticationHeader(), this.apiVersion);
        return this.http.delete(`${this.baseUrl}/${this.apiVersion}/${url}`, config)
          .map((res:any) => res).toPromise();
      }
    );
  }
}
