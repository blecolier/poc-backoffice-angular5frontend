import {Http, Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class ConfigService {

  constructor(private http:HttpClient) {
    this.http.get('/assets/app.config.json')
      .map((res:any) => localStorage.cfg = JSON.stringify(res));
  }

  public getConfiguration(): Observable<any>{
    let config = JSON.parse(localStorage.getItem('cfg'));
    return Observable.of(config);
  }
}
