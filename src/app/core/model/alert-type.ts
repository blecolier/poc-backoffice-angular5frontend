export class Alert {
    type: AlertType;
    title: string;
    messages: string[];
    callback: any;
}

export enum AlertType {
    Success,
    Error,
    Info,
    Warning
}
