export class FormValidationResult {
    isValid: boolean;
    messages: string[];

    constructor(){
      this.isValid = true;
      this.messages = [];
    }
}
