import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import { Observable } from 'rxjs';
import {Alert, AlertType} from './model/alert-type';

@Injectable()
export class CommunicationService {
  private onAlert = new Subject<Alert>();
  private onModal = new Subject<Alert>();

  getAlert(): Observable<any> {
    return this.onAlert.asObservable();
  }

  getModal(): Observable<any>{
    return this.onModal.asObservable();
  }

  confirm(title: string, message: string, callback: any){
    this.onModal.next(<Alert>{ messages: [message], title: title, callback: callback });
  }

  success(title: string, messages: string[]){
    this.alert(AlertType.Success, messages, title);
  }

  error(title: string, messages: string[]){
    this.alert(AlertType.Error, messages, title);
  }

  info(title: string, messages: string[]){
    this.alert(AlertType.Info, messages, title);
  }

  alert(type: AlertType, messages: string[], title: string) {
    this.onAlert.next(<Alert>{ type: type, messages: messages, title: title });
  }

  clear() {
    // clear alerts
    //this.onAlert.next();
  }
}
