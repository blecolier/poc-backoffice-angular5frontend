import {Injectable} from '@angular/core';
import {CanActivateChild, Router} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class CanActivateChildAuthGuard implements CanActivateChild {
  constructor (private authService: AuthService, private router: Router){}

  canActivateChild(){
    if(this.authService.verifyAuthSynchrone()){
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
