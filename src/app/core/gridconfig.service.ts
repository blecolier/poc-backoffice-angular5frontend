import { Injectable } from '@angular/core';
import { Config } from './model/ngx-table-config';

@Injectable()
export class ConfigService {
  public static config: Config = {
    searchEnabled: false,
    headerEnabled: true,
    orderEnabled: false,
    globalSearchEnabled: false,
    paginationEnabled: true,
    exportEnabled: false,
    clickEvent: false,
    selectRow: false,
    selectCol: false,
    selectCell: false,
    rows: 10,
    additionalActions: false,
    serverPagination: true,
    isLoading: false,
    detailsTemplate: false,
    groupRows: false,
    paginationRangeEnabled: true,
    collapseAllRows: false,
    checkboxes: false
  };
}

export class PaginationFactory{
  static CreatePagination(pagination: any) : string{
    return `_limit=${pagination.limit}&_offset=${pagination.offset}`;
  }
}

export interface EventObject {
  event: string;
  value: any;
}
