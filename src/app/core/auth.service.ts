import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthCurrentService } from './auth.current.service';
import {ConfigService} from './config.service';

function needToRefreshToken(token: string, expirationDate: number, tokenLimit: number){
  if(!token || expirationDate === 0)
    return false;

  // token expiration property is defined in seconds according to the RFC
  const date = Date.now() / 1000 + tokenLimit * 60;
  return expirationDate < date;
}

function hasTokenExpired(expirationDate: number){
  if(expirationDate === 0)
    return true;

  return expirationDate <= Date.now() / 1000;
}


@Injectable()
export class AuthService{
  private baseUrl: string;
  private timeLimit: number;

  private config: any;

  constructor(private http: HttpClient, private authCurrentService: AuthCurrentService, appConfig: ConfigService){
    appConfig.getConfiguration()
      .subscribe(data => {
        this.baseUrl = data.Api.baseUrl;
        this.timeLimit = data.Api.tokenExpirationLimit;
      }, error => console.log(error));
  }

  getAuthenticationHeader(){
    return this.authCurrentService.getAuthenticationHeader();
  }

  refreshTokenIfNecessary(){
    if(!needToRefreshToken(this.authCurrentService.getCurrentToken(), this.authCurrentService.getTokenExpirationDate(), this.timeLimit)){
      return Promise.resolve({});
    }

    return this.http.post<any>(`${this.baseUrl}/refresh`, { }, {headers:{Authorization: this.getAuthenticationHeader()}})
      .toPromise()
      .then((result) => {
          this.authCurrentService.setCurrentToken(result.token);
          return Promise.resolve(result);
        }
      );
  }

  logout(){
    this.http.post<any>(`${this.baseUrl}/logout`, { }, {headers:{Authorization: this.getAuthenticationHeader()}})
      .toPromise()
      .then(() => {
          this.authCurrentService.destroyToken();
        }
    );
  }

  authenticate(credentials, callback){
    this.http.post<any>(`${this.baseUrl}/login`, { credentials : credentials })
    .toPromise()
    .then((result) => {
        this.authCurrentService.setCurrentToken(result.token);
        callback(true);
      },
      () => { callback(null); }
    );
  }

  verifyAuthSynchrone(){
    let token = this.authCurrentService.getCurrentToken();

    if(token !== undefined && token !== null && token !== '' && !hasTokenExpired(this.authCurrentService.getTokenExpirationDate())){
      return true;
    }

    this.authCurrentService.destroyToken();
    return false;
  }

  verifyAuth(){
    let token = this.authCurrentService.getCurrentToken();

    if(token !== undefined && token !== null && token !== '' && !hasTokenExpired(this.authCurrentService.getTokenExpirationDate())){
      return Promise.resolve(true);
    }
    else {
      this.authCurrentService.destroyToken();
      return Promise.resolve(false);
    }
  }
}
