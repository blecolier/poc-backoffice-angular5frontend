import { Component, Output, EventEmitter } from '@angular/core';
import { AppService } from './app.service';
import { CommunicationService } from './app.communication.service';


export class AdminComponent{
  constructor(protected appService: AppService, protected communicationService: CommunicationService, componentTitle: string) {
    appService.reloadAppTitle(componentTitle);
  }
}
