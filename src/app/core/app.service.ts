import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import { AuthCurrentService } from './auth.current.service';
import {ConfigService} from './config.service';

@Injectable()
export class AppService {
  applicationEnvironment: string;
  titleChanged = new Subject<string>();

  constructor(private authCurrentService: AuthCurrentService, appConfig: ConfigService) {
    appConfig.getConfiguration()
      .subscribe(data => {
        this.applicationEnvironment = data.ApplicationEnvironment;
      }, error => console.log(error));
  }

  reloadAppTitle(appTitle: string){
    this.titleChanged.next(appTitle);
  }

  getAppEnvironment() {
    return this.applicationEnvironment;
  }

  userLogedIn(){
    localStorage.currentUser = JSON.stringify(this.authCurrentService.extractUserFromToken());
    localStorage.logedIn = true;
  }

  getUserModules(){
    return localStorage.currentUser ? JSON.parse(localStorage.getItem('currentUser')).modules : [];
  }

  getUserName(){
    return localStorage.currentUser ? JSON.parse(localStorage.getItem('currentUser')).displayName : '';
  }
}
