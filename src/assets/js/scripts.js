//$.widget.bridge('uibutton', $.ui.button);

//receive calls from typescript code to update the layouts
var AdminLTE = (function() {
  return {
    init: function() {
      $(function(){
        var $layout = $('body').data('lte.layout')
        $layout.fixSidebar();
        $layout.fix();
        $layout.activate();

        $('[data-widget="tree"]').tree();
        $('[data-widget="collapse"]').boxWidget('toggle');
      });
    }
  }
})(AdminLTE||{});

var jquery = (function(){
  return{
    setTimeout: function(seconds, callback){
      $(function(){
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                callback();
            });
        }, seconds * 1000);
      })
    },

    showModal: function(modalName){
      $(function(){
        $('#'+modalName).modal({
          keyboard: false,
          focus: true,
          show: true,
          backdrop: 'static'
        });
        $('#'+modalName).modal('toggle');
      })
    },

    hideModal: function(modalName){
      $(function(){
        $('#'+modalName).modal('toggle');
      })
    }
  }
})(jquery||{});
